<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Larasoft\LaravelRemote\LaravelRemoteJobFailed;
use Mockery\CountValidator\Exception;

class Test1 implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        throw new Exception('Something wrong');
    }

    /**
     * Fail the job from the queue.
     *
     * @return void
     */
    public function failed()
    {
        $data = ['job' => class_basename($this)];
        event(new LaravelRemoteJobFailed($data));
    }

}
